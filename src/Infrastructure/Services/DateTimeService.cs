﻿using social_media_be.Application.Common.Interfaces;

namespace social_media_be.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}
