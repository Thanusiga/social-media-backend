using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations;
public class ShareConfiguration : IEntityTypeConfiguration<Share>
{
    public void Configure(EntityTypeBuilder<Share> builder)
    {
        builder.HasKey(i => i.Id);

        builder.Property(i => i.Id)
                .ValueGeneratedOnAdd();

        builder.Property(i => i.PostId)
                        .IsRequired();

        builder.Property(i => i.SharedBy)
                        .IsRequired();

        builder.Property(i => i.SharedDateTime)
                        .IsRequired();

        builder.Property(i => i.IsActive)
                        .IsRequired();

        builder
            .HasOne(x => x.Post)
            .WithMany(x => x.Shares)
            .HasForeignKey(x => x.PostId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.Restrict)
            .IsRequired();

    }
}


// could not user asp.net user as a ref for user, it should has a m*m relationship