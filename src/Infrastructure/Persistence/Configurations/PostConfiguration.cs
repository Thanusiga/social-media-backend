using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations;

public class PostConfiguration : IEntityTypeConfiguration<Post>
{
    public void Configure(EntityTypeBuilder<Post> builder)
    {
        builder.HasKey(i => i.Id);

        builder.Property(i => i.Id)
                .ValueGeneratedOnAdd();

        builder.Property(i => i.UserId)
                        .IsRequired();

        builder.Property(i => i.PostedDateTime)
                        .IsRequired();

        builder.Property(i => i.IsActive)
                        .IsRequired();

        builder.Property(i => i.Images)
            .HasColumnType("jsonb");

        builder.Property(i => i.Videos)
            .HasColumnType("jsonb");
    }
}

// could not user asp.net user as a ref for user, it should has a m*m relationship