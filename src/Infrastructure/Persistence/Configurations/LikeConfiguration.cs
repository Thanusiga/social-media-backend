using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations;
public class LikeConfiguration : IEntityTypeConfiguration<Like>
{
    public void Configure(EntityTypeBuilder<Like> builder)
    {
        builder.HasKey(i => i.Id);

        builder.Property(i => i.Id)
                .ValueGeneratedOnAdd();

        builder.Property(i => i.PostId)
                        .IsRequired();

        builder.Property(i => i.LikedBy)
                        .IsRequired();

        builder.Property(i => i.LikedDateTime)
                        .IsRequired();

        builder.Property(i => i.IsActive)
                        .IsRequired();

        builder
            .HasOne(x => x.Post)
            .WithMany(x => x.Likes)
            .HasForeignKey(x => x.PostId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.Restrict)
            .IsRequired();
    }
}
