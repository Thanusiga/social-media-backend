using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations;
public class CommentConfiguration : IEntityTypeConfiguration<Comment>
{
    public void Configure(EntityTypeBuilder<Comment> builder)
    {
        builder.HasKey(i => i.Id);

        builder.Property(i => i.Id)
                .ValueGeneratedOnAdd();

        builder.Property(i => i.PostId)
                        .IsRequired();

        builder.Property(i => i.CommentedBy)
                        .IsRequired();

        builder.Property(i => i.CommentedDateTime)
                        .IsRequired();

        builder.Property(i => i.IsActive)
                        .IsRequired();

        builder
            .HasOne(x => x.Post)
            .WithMany(x => x.Comments)
            .HasForeignKey(x => x.PostId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.Restrict)
            .IsRequired();

    }
}

// could not user asp.net user as a ref for user, it should has a m*m relationship