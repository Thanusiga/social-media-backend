﻿using Microsoft.AspNetCore.Identity;

namespace social_media_be.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Profile { get; set; }
    public Boolean IsActive { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime EditedOn { get; set; }

}
