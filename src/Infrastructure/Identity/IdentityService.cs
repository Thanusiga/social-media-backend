﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using social_media_be.Infrastructure.Persistence;
using social_media_be.Application.Common.Models;
using social_media_be.Application.Common.Interfaces;

namespace social_media_be.Infrastructure.Identity;

public class IdentityService : IIdentityService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IUserClaimsPrincipalFactory<ApplicationUser> _userClaimsPrincipalFactory;
    private readonly IAuthorizationService _authorizationService;
    private readonly ApplicationDbContext _applicationDbContext;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IPasswordHasher<ApplicationUser> _passwordHasher;


    public IdentityService(
        UserManager<ApplicationUser> userManager,
        IUserClaimsPrincipalFactory<ApplicationUser> userClaimsPrincipalFactory,
        IAuthorizationService authorizationService,
        ApplicationDbContext applicationDbContext,
        RoleManager<IdentityRole> roleManager,
        SignInManager<ApplicationUser> signInManager,
        IHttpContextAccessor httpContextAccessor,

        IPasswordHasher<ApplicationUser> passwordHasher)
    {
        _userManager = userManager;
        _userClaimsPrincipalFactory = userClaimsPrincipalFactory;
        _authorizationService = authorizationService;
        _applicationDbContext = applicationDbContext;
        _roleManager = roleManager;
        _signInManager = signInManager;
        _httpContextAccessor = httpContextAccessor;
        _passwordHasher = passwordHasher;

    }

    public async Task<string> GetUserNameAsync(string userId)
    {
        var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

        return user.UserName;
    }

    public async Task<bool> IsInRoleAsync(string userId, string role)
    {
        var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        return user != null && await _userManager.IsInRoleAsync(user, role);
    }

    public async Task<bool> AuthorizeAsync(string userId, string policyName)
    {
        var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        if (user == null)
        {
            return false;
        }

        var principal = await _userClaimsPrincipalFactory.CreateAsync(user);

        var result = await _authorizationService.AuthorizeAsync(principal, policyName);

        return result.Succeeded;
    }

    public async Task<Result> DeleteUserAsync(string userId)
    {
        var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        return user != null ? await DeleteUserAsync(user) : Result.Success();
    }

    public async Task<Result> DeleteUserAsync(ApplicationUser user)
    {
        var result = await _userManager.DeleteAsync(user);

        return result.ToApplicationResult();
    }

    public async Task<User> FindByEmailAsync(string email)
    {

        var result = await _userManager.FindByEmailAsync(email);
        if (result == null)
        {
            throw new UnauthorizedAccessException();
        }
        var user = new User();


        user.FirstName = result.FirstName;
        user.LastName = result.LastName;
        user.Profile = result.Profile;
        user.IsActive = result.IsActive;
        user.CreatedOn = result.CreatedOn;
        user.EditedOn = result.EditedOn;

        user.UserName = result.UserName;
        user.Email = result.Email;
        user.Id = result.Id;
        user.AccessFailedCount = result.AccessFailedCount;
        user.ConcurrencyStamp = result.ConcurrencyStamp;
        user.EmailConfirmed = result.EmailConfirmed;
        user.PhoneNumber = result.PhoneNumber;
        user.PasswordHash = result.PasswordHash;
        user.NormalizedUserName = result.NormalizedUserName;
        user.NormalizedEmail = result.NormalizedEmail;
        user.LockoutEnabled = result.LockoutEnabled;
        user.LockoutEnd = result.LockoutEnd;
        user.PhoneNumberConfirmed = result.PhoneNumberConfirmed;
        user.SecurityStamp = result.SecurityStamp;
        user.TwoFactorEnabled = result.TwoFactorEnabled;


        return user;
    }

    public async Task<bool> IsEmailConfirmedAsync(User result)
    {

        var user = new ApplicationUser();

        user.FirstName = result.FirstName;
        user.LastName = result.LastName;
        user.Profile = result.Profile;
        user.IsActive = result.IsActive;
        user.CreatedOn = result.CreatedOn;
        user.EditedOn = result.EditedOn;

        user.UserName = result.UserName;
        user.Email = result.Email;
        user.Id = result.Id;

        user.AccessFailedCount = result.AccessFailedCount;
        user.ConcurrencyStamp = result.ConcurrencyStamp;
        user.EmailConfirmed = result.EmailConfirmed;
        user.PhoneNumber = result.PhoneNumber;
        user.PasswordHash = result.PasswordHash;
        user.NormalizedUserName = result.NormalizedUserName;
        user.NormalizedEmail = result.NormalizedEmail;
        user.LockoutEnabled = result.LockoutEnabled;
        user.LockoutEnd = result.LockoutEnd;
        user.PhoneNumberConfirmed = result.PhoneNumberConfirmed;
        user.SecurityStamp = result.SecurityStamp;
        user.TwoFactorEnabled = result.TwoFactorEnabled;

        var conformed = await _userManager.IsEmailConfirmedAsync(user);

        return conformed;
    }


    public async Task<social_media_be.Application.Common.Interfaces.IdentityResult> CreateAsync(User user, string password)
    {
        var applicationUser = new ApplicationUser();

        applicationUser.FirstName = user.FirstName;
        applicationUser.LastName = user.LastName;
        applicationUser.Profile = user.Profile;
        applicationUser.IsActive = user.IsActive;
        applicationUser.CreatedOn = user.CreatedOn;
        applicationUser.EditedOn = user.EditedOn;

        applicationUser.UserName = user.UserName;
        applicationUser.Email = user.Email;
        applicationUser.Id = user.Id;
        applicationUser.AccessFailedCount = user.AccessFailedCount;
        applicationUser.ConcurrencyStamp = user.ConcurrencyStamp;
        applicationUser.EmailConfirmed = user.EmailConfirmed;
        applicationUser.PhoneNumber = user.PhoneNumber;
        applicationUser.PasswordHash = user.PasswordHash;
        applicationUser.NormalizedUserName = user.NormalizedUserName;
        applicationUser.NormalizedEmail = user.NormalizedEmail;
        applicationUser.LockoutEnabled = user.LockoutEnabled;
        applicationUser.LockoutEnd = user.LockoutEnd;
        applicationUser.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
        applicationUser.SecurityStamp = user.SecurityStamp;
        applicationUser.TwoFactorEnabled = user.TwoFactorEnabled;

        var result = await _userManager.CreateAsync(applicationUser, password);

        var res = new social_media_be.Application.Common.Interfaces.IdentityResult() { };
        // var x = result.Errors;

        res.Succeeded = result.Succeeded;
        res.ErrorMessages = result.Errors.Select(x => x.Description).ToList();

        var z = result.ToString;

        return res;
    }

    public Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
    {
        throw new NotImplementedException();
    }

    public async Task<User> FindByNameAsync(string userName)
    {
        var result = await _userManager.FindByNameAsync(userName);
        if (result == null)
        {
            throw new UnauthorizedAccessException();
        }
        var user = new User();

        user.FirstName = result.FirstName;
        user.LastName = result.LastName;
        user.Profile = result.Profile;
        user.IsActive = result.IsActive;
        user.CreatedOn = result.CreatedOn;
        user.EditedOn = result.EditedOn;

        user.UserName = result.UserName;
        user.Email = result.Email;
        user.Id = result.Id;
        user.AccessFailedCount = result.AccessFailedCount;
        user.ConcurrencyStamp = result.ConcurrencyStamp;
        user.EmailConfirmed = result.EmailConfirmed;
        user.PhoneNumber = result.PhoneNumber;
        user.PasswordHash = result.PasswordHash;
        user.NormalizedUserName = result.NormalizedUserName;
        user.NormalizedEmail = result.NormalizedEmail;
        user.LockoutEnabled = result.LockoutEnabled;
        user.LockoutEnd = result.LockoutEnd;
        user.PhoneNumberConfirmed = result.PhoneNumberConfirmed;
        user.SecurityStamp = result.SecurityStamp;
        user.TwoFactorEnabled = result.TwoFactorEnabled;


        return user;
    }

    public Task<bool> CheckPasswordAsync(User user, string password)
    {
        var applicationUser = new ApplicationUser();

        applicationUser.FirstName = user.FirstName;
        applicationUser.LastName = user.LastName;
        applicationUser.Profile = user.Profile;
        applicationUser.IsActive = user.IsActive;
        applicationUser.CreatedOn = user.CreatedOn;
        applicationUser.EditedOn = user.EditedOn;

        applicationUser.UserName = user.UserName;
        applicationUser.Email = user.Email;
        applicationUser.Id = user.Id;
        applicationUser.AccessFailedCount = user.AccessFailedCount;
        applicationUser.ConcurrencyStamp = user.ConcurrencyStamp;
        applicationUser.EmailConfirmed = user.EmailConfirmed;
        applicationUser.PhoneNumber = user.PhoneNumber;
        applicationUser.PasswordHash = user.PasswordHash;
        applicationUser.NormalizedUserName = user.NormalizedUserName;
        applicationUser.NormalizedEmail = user.NormalizedEmail;
        applicationUser.LockoutEnabled = user.LockoutEnabled;
        applicationUser.LockoutEnd = user.LockoutEnd;
        applicationUser.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
        applicationUser.SecurityStamp = user.SecurityStamp;
        applicationUser.TwoFactorEnabled = user.TwoFactorEnabled;

        var result = _userManager.CheckPasswordAsync(applicationUser, password);

        return result;

    }

    public Task<IList<string>> GetRolesAsync(User user)
    {
        var applicationUser = new ApplicationUser();

        applicationUser.FirstName = user.FirstName;
        applicationUser.LastName = user.LastName;
        applicationUser.Profile = user.Profile;
        applicationUser.IsActive = user.IsActive;
        applicationUser.CreatedOn = user.CreatedOn;
        applicationUser.EditedOn = user.EditedOn;

        applicationUser.UserName = user.UserName;
        applicationUser.Email = user.Email;
        applicationUser.Id = user.Id;
        applicationUser.AccessFailedCount = user.AccessFailedCount;
        applicationUser.ConcurrencyStamp = user.ConcurrencyStamp;
        applicationUser.EmailConfirmed = user.EmailConfirmed;
        applicationUser.PhoneNumber = user.PhoneNumber;
        applicationUser.PasswordHash = user.PasswordHash;
        applicationUser.NormalizedUserName = user.NormalizedUserName;
        applicationUser.NormalizedEmail = user.NormalizedEmail;
        applicationUser.LockoutEnabled = user.LockoutEnabled;
        applicationUser.LockoutEnd = user.LockoutEnd;
        applicationUser.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
        applicationUser.SecurityStamp = user.SecurityStamp;
        applicationUser.TwoFactorEnabled = user.TwoFactorEnabled;

        var result = _userManager.GetRolesAsync(applicationUser);

        return result;
    }
}

