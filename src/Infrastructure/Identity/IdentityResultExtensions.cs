﻿using Microsoft.AspNetCore.Identity;
using social_media_be.Application.Common.Models;

namespace social_media_be.Infrastructure.Identity;

public static class IdentityResultExtensions
{
    public static Result ToApplicationResult(this IdentityResult result)
    {
        return result.Succeeded
            ? Result.Success()
            : Result.Failure(result.Errors.Select(e => e.Description));
    }
}
