namespace Domain.Entities;
public class Post
{
    public int Id { get; set; }
    public int UserId { get; set; }  // by who
    public string Content { get; set; }
    public List<PostImages> Images { get; set; }  // jsonb
    public List<PostVideos> Videos { get; set; } // jsonb
    public DateTime PostedDateTime { get; set; }
    public DateTime LastEditedDateTime { get; set; }
    public Boolean IsActive { get; set; }

    public List<Like> Likes { get; set; }
    public List<Comment> Comments { get; set; }
    public List<Share> Shares { get; set; }

}

public class PostImages
{
    public string Images { get; set; }
}
public class PostVideos
{
    public string Videos { get; set; }
}