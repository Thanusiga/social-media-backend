namespace Domain.Entities;

public class Like
{
    public int Id { get; set; }
    public int PostId { get; set; }       // ref post  ----> by who      m
    public int LikedBy { get; set; }      // ref user         m
    public DateTime LikedDateTime { get; set; }
    public Boolean IsActive { get; set; }

    public Post Post { get; set; }
}