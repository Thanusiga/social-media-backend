namespace Domain.Entities;
public class Share
{
    public int Id { get; set; }
    public int PostId { get; set; }   // ref post    m
    public int SharedBy { get; set; }         // ref user          m
    public DateTime SharedDateTime { get; set; }
    public int IsActive { get; set; }
    public Post Post { get; set; }
}
