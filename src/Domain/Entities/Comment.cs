namespace Domain.Entities;
public class Comment
{
    public int Id { get; set; }
    public int PostId { get; set; } // ref post m
    public int CommentedBy { get; set; }     // ref user m
    public DateTime CommentedDateTime { get; set; }
    public DateTime LastEditedDateTime { get; set; }
    public Boolean IsActive { get; set; }

    public Post Post { get; set; }
    // public List<User> Users { get; set; }
}


// done