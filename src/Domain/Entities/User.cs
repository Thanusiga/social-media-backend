
namespace Domain.Entities;
public class User
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Profile { get; set; }
    public Boolean IsActive { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime EditedOn { get; set; }

    public int AccessFailedCount { get; set; }
    public string ConcurrencyStamp { get; set; }
    public string PhoneNumber { get; set; }
    public bool PhoneNumberConfirmed { get; set; }

    public string Email { get; set; }
    public bool EmailConfirmed { get; set; }

    public string UserName { get; set; }
    public bool IsAdmin { get; set; }

    public string Password { get; set; }
    public string PasswordHash { get; set; }
    public string NormalizedUserName { get; set; }
    public string NormalizedEmail { get; set; }
    public string SecurityStamp { get; set; }
    public bool TwoFactorEnabled { get; set; }
    public bool LockoutEnabled { get; set; }
    public DateTimeOffset? LockoutEnd { get; set; }
    public string Id { get; set; }
    // public List<string> Roles { get; set; }
    // public List<int> UserRoles { get; set; }

}

// public enum UserType : int
// {
//     SuperAdmin = 1,
//     Admin = 2,
//     Customer = 3
// }

// public enum RecordState : byte
// {
//     Active = 1,
//     Inactive = 2
// }