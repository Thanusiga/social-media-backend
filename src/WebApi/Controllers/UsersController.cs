using Application.Common.Models.ViewModels;
using Application.Login;
using Application.Register;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

public class UsersController : ApiControllerBase
{

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<ActionResult<string>> Register([FromForm] RegisterCommand command)
    {
        var result = await Mediator.Send(command);

        return Ok(result);
    }

    [HttpPost]
    [Route("login")]
    public async Task<ActionResult<LoginViewModel>> Login([FromBody] LoginCommand command)
    {
        var result = await Mediator.Send(command);

        return Ok(result);
    }
}