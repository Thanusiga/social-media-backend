using FluentValidation;

namespace Application.Register;
public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
{
    public RegisterCommandValidator()
    {
        RuleFor(i => i.Email)
        .EmailAddress()
        .NotNull().WithMessage("Required");

        RuleFor(i => i.FirstName)
        .NotNull().WithMessage("First Name is required.");

        RuleFor(i => i.Password)
        .NotNull().WithMessage("Password is required.");

        RuleFor(i => i.ConfirmedPassword)
        .NotNull().WithMessage("Password is required.");


    }
}
