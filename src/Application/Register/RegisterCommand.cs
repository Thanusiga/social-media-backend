using Application.Common.Models.Dto;
using AutoMapper;
using Domain.Entities;
using MediatR;
using social_media_be.Application.Common.Exceptions;
using social_media_be.Application.Common.Interfaces;

namespace Application.Register
{
    public class RegisterCommand : RegisterUserDto, IRequest<string>
    {

    }


    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, string>
    {

        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _applicationDbContext;
        private readonly IIdentityService _identityService;
        public RegisterCommandHandler(IIdentityService identityService, IMapper mapper, IApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            _identityService = identityService;
            _mapper = mapper;
        }
        public async Task<string> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var mailExists = await _identityService.FindByEmailAsync(request.Username);
            var userExists = await _identityService.FindByNameAsync(request.Username);
            if (userExists != null)
            {
                throw new AlreadyExistException("User name already exists");
            }
            else if (mailExists != null)
            {
                throw new AlreadyExistException("Email already exists");
            }

            var user = new User
            {
                Email = request.Email,
                UserName = request.Username
            };

            var result = await _identityService.CreateAsync(user, request.Password);
            if (!result.Succeeded)
            {
                throw new Exception(result.ErrorMessages[0]);
            }
            // return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });

            return result.ErrorMessages[0];

        }

    }
}