namespace Application.Common.Models.ViewModels
{
    public class LoginViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public IList<string> Roles { get; set; }
    }
}