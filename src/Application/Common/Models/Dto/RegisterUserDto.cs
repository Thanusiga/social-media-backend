namespace Application.Common.Models.Dto;
public class RegisterUserDto
{

    public string FirstName { get; set; }

    public string LastName { get; set; }
    // [Required(ErrorMessage = "User Name is required")]
    public string Username { get; set; }

    // [EmailAddress]
    // [Required(ErrorMessage = "Email is required")]
    public string Email { get; set; }

    // [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; }

    public string ConfirmedPassword { get; set; }

    public string Profile { get; set; }

}


// have to validate - todo