namespace Application.Common.Models.Dto;
public class UserDto
{
    public string Email { get; set; }
    public string UserName { get; set; }

    public string Password { get; set; }

    public List<string> Roles { get; set; }
    public List<int> SiteCode { get; set; }
}
