﻿using Application.Common.Models.Dto;
using Domain.Entities;
using social_media_be.Application.Common.Models;

namespace social_media_be.Application.Common.Interfaces;

public interface IIdentityService
{
    Task<string> GetUserNameAsync(string userId);

    Task<bool> IsInRoleAsync(string userId, string role);

    Task<bool> AuthorizeAsync(string userId, string policyName);

    Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password);

    Task<Result> DeleteUserAsync(string userId);
    Task<IdentityResult> CreateAsync(User user, string password);

    Task<bool> CheckPasswordAsync(User user, string password);

    Task<User> FindByEmailAsync(string email);

    Task<User> FindByNameAsync(string userName);
    Task<bool> IsEmailConfirmedAsync(User user);

    Task<IList<string>> GetRolesAsync(User user);

}


public class IdentityResult
{
    public Boolean Succeeded { get; set; }

    public List<string> ErrorMessages { get; set; }

}