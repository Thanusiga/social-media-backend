using System.Security.Claims;
using Domain.Entities;

namespace Application.Common.Interfaces;
public interface IJwtService
{
    string GenerateJwtSecurityToken(List<Claim> authClaims);

}