﻿namespace social_media_be.Application.Common.Interfaces;

public interface ICurrentUserService
{
    string UserId { get; }
}
