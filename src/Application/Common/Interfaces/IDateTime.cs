﻿namespace social_media_be.Application.Common.Interfaces;

public interface IDateTime
{
    DateTime Now { get; }
}
