﻿using social_media_be.Domain.Common;

namespace social_media_be.Application.Common.Interfaces;

public interface IDomainEventService
{
    Task Publish(DomainEvent domainEvent);
}
