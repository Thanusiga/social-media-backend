﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace social_media_be.Application.Common.Interfaces;

public interface IApplicationDbContext
{

    public DbSet<Like> Likes { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Share> Shares { get; set; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
