using System.Security.Claims;
using Application.Common.Interfaces;
using Application.Common.Models.Dto;
using Application.Common.Models.ViewModels;
using MediatR;
using social_media_be.Application.Common.Interfaces;

namespace Application.Login;

public record LoginCommand(string Username, string Password) : IRequest<LoginViewModel>;

public class LoginCommandHandler : IRequestHandler<LoginCommand, LoginViewModel>
{

    private readonly IIdentityService _identityService;

    private readonly IJwtService _jwtService;

    public LoginCommandHandler(IIdentityService identityService, IJwtService jwtService)
    {
        _jwtService = jwtService;
        _identityService = identityService;

    }
    public async Task<LoginViewModel> Handle(LoginCommand request, CancellationToken cancellationToken)
    {

        var user = await _identityService.FindByNameAsync(request.Username);
        if (user != null && await _identityService.CheckPasswordAsync(user, request.Password))
        {
            var userRoles = await _identityService.GetRolesAsync(user);

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                };

            foreach (string userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            var token = _jwtService.GenerateJwtSecurityToken(authClaims);

            return new LoginViewModel()
            {
                Token = token,
                Email = user.Email,
                UserId = user.Id,
                UserName = user.UserName,
                Roles = userRoles,
            };
        }
        else
        {
            throw new UnauthorizedAccessException("Your user name or password is incorrect");
        }

    }


}
