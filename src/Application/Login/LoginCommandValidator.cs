using FluentValidation;

namespace Application.Login;
public class LoginCommandValidator : AbstractValidator<LoginCommand>
{
    public LoginCommandValidator()
    {
        RuleFor(i => i.Username)
        .EmailAddress()
        .NotNull().WithMessage("Required");


        RuleFor(i => i.Password)
        .NotNull().WithMessage("Password is required.");


    }

}
